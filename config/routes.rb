Rails.application.routes.draw do
  get 'statistics/playbacks_by_artist', to: "statistics#playbacks_by_artist", as: :root
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
