# Web Suena Chile:
### Visualización de la estadística de reproducciones totales por cada artista.

##### Requerimientos mínimos:
- Ruby 2.5.5
- Rails 5.2.3
Para la gestión de la versión de Ruby y Rails se ha utilizado **RVM** pero se puede utilizar **rbenv** si es de su preferencia.

Pasos para la ejecución:

1. Clonar el repositorio.
2. Estando en la cónsola de su preferencia (iTerm, Terminal, etc) y con el intérprete de su preferencia (bash, zsh, etc), ejecutar **bundle install¨** para la instalación de todas las dependencias.
3. Al tener datos cargados, puede ejecutar el servidor de prueba a partir del comando **rails s -p3001** en la cónsola. Evite utilizar el puerto 3000, dado a que ya está en uso por la API.
3. Para ver los resultados vaya a la url http://localhost:3001/statistics/playbacks_by_artist