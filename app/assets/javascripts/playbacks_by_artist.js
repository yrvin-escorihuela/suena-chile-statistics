$(document).ready(function(){
    refresh();
});

var refresh = () => {
  $.ajax({
    type: 'GET',
    url: 'http://localhost:3000/api/v1/statistics/playbacks_by_artist/?r=' + Math.floor((Math.random() * 1234567890)) ,
    success: function(data) {

      var thisChart = echarts.init(document.getElementById('main'));
      var legend_data = [];
      var series_data = [];
      data.map((e) => {
        legend_data.push(e.artist_name);
        series_data.push(e.total_playbacks);
      })

      var option = {
          title: {
              text: 'Top Ten Artistas Musicales de Chile'
          },
          tooltip: {},
          legend: {
              data: legend_data
          },
          xAxis: {
              data: legend_data
          },
          yAxis: {},
          series: [{
              name: 'Reproducciones totales',
              type: 'bar',
              data: series_data
          }]
      };

      thisChart.setOption(option);


    }
  });
}

